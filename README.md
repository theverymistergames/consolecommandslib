﻿﻿﻿# MisterGames ConsoleCommandsLib v0.1.0

## Features
- todo

## Assembly definitions
- MisterGames.ConsoleCommandsLib

## Dependencies
- MisterGames.Common
- MisterGames.Dbg
- MisterGames.Scenes
- MisterGames.Character

## Installation 
- Add [MisterGames Common](https://gitlab.com/theverymistergames/common) package
- Top menu MisterGames -> Packages, add packages: 
  - [Dbg](https://gitlab.com/theverymistergames/dbg/)
  - [Scenes](https://gitlab.com/theverymistergames/scenes/)
  - [Character](https://gitlab.com/theverymistergames/character/)
  - [ConsoleCommandsLib](https://gitlab.com/theverymistergames/consolecommandslib/)